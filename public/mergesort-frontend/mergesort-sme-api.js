

/// the Mergesort datatype

(function(){


     var split = function (inputArray){
        var middle = parseInt(inputArray.length / 2);
        var left   = inputArray.slice(0, middle);
        var right  = inputArray.slice(middle, inputArray.length);

        return [left, right];
    }

    var merge = function(left, right)
    {
        var result = [];
        //console.log(result.length, "results length");
        while (left.length && right.length) {
            if (left[0] <= right[0]) {
                //console.log(left[0], right[0], "first elements of left and right");
                result.push(left.shift());
                //console.log(result, "mergesort: left shift: check result");
            } else {
                result.push(right.shift());
                //console.log(result, "mergesort: right shift: check result");
            }
        }
     
        while (left.length)
            result.push(left.shift());
        //console.log(result, "mergesort: left shift: check result - 2nd while");
     
        while (right.length)
            result.push(right.shift());
        //console.log(result, "mergesort: right shift: check result - 2nd while");
        return result;
    }

    var mergeSort = function (inputArray)
    {
        //console.log('switch value', switch);
        if (inputArray.length < 2)
            
            return inputArray;

        //two arrays, index:0 = left array
        // index:1 = right array
        var splitArray = split(inputArray);
        
        //console.log({[middle]: [left,right]},"SPLIT: checkleftrightmiddle");
        // return the the split arrays
        return merge(mergeSort(splitArray[0]), mergeSort(splitArray[1]));
        
    }


    // Data Type E.Mergesort with methods 
    // split, merge, and sort
    //Mergesort.data =[]


    Mergesort = function(data){
        this.data = data || [1, 4, 2, 6, 3];
    }

    //E.Mergesort.prototype.constructor = Object.create(E.Mergesort.prototype)
    Mergesort.prototype = {
        setData: function(arg){
        //arg is array
        return this.data = arg;

    },
        split: function(){
                return split(this.data);
    },
        merge: function(){
            return merge(this.data[0], this.data[1]);
    },
        sort: function(){
            return mergeSort(this.data, 'mergesort');
    }

    }

  /*  Mergesort.prototype.setData = function(arg){
        //arg is array
        return this.data = arg;

    }

    Mergesort.prototype.split = function(){
            return split(this.data);
    }

    Mergesort.prototype.merge = function(){
        return merge(this.data[0], this.data[1]);
    }

    Mergesort.prototype.sort = function(){
        return mergeSort(this.data, 'mergesort');
    }

    
*/
})();

