
/* Evaluator function:
Purpose: To evaluate a user input in a certain context
Context: input value, split or merge sort
compare: user input value with the result
result: function to merge or merge sort based on context
*/

/*
usecase: user enters a list in the input box,
in the context to test split function
unsorted array,
and user input

inputs = 2 arrays

Example:
Split: Question will have a list, answer will have two lists
so eveluate call will be
E.EvaluateSplit([[int array left],[Int array right]], [question arr], 'split')
merge two sorted lists
E.EvaluateMerge([answer int array], [[qleft], [qright]], 'merge')
mergesort
E.EvaluateMergesort([answer], [question], 'mergesort')
*/
(function(Mergesort){
console.log("E init");
E = window.E || {};
E.Utilities = E.Utilities || {};



// E.Evaluator with the userinput [[], []], question [], ctx 'string' split or 'merge'
// or 'merge sort'

E.Evaluator = function(uInput, inputArray, ctx){
	// new local instance of the Mergesort datatype
	// Evaluator will create new instance everytime it is called
	var ms = new Mergesort(inputArray);

	//console.log(uInput, inputArray, ctx, "ux pay load");

	/*
	  EVALUATE SPLIT FUNCTION
	  INPUT: Two arrays : User input array [[left elements],[right elements]]
	  					  Input array already defined[ n array of elements]
	  OUTPUT : Boolean value after comparision
	  IMPLEMENTATION:Perform split with the array already defined and compare the split result
	  with the user input array and return boolean value.
	*/
	var evaluateSplit = function(userInput, inputArray){
		var comparedResultL, comparedResultR;
		comparedResultL = E.Utilities.array.isEqual(ms.split(inputArray)[0], userInput[0]);
		comparedResultR = E.Utilities.array.isEqual(ms.split(inputArray)[1], userInput[1]);
		console.log(comparedResultL, ms.split(inputArray)[0], userInput[0], comparedResultR, ms.split(inputArray)[1], userInput[1]);
		return comparedResultR && comparedResultL;
	}
	/*
	  EVALUATE MERGE FUNCTION
	  INPUT: Two arrays : User input array [array of n elements]
	  					  Input array already defined[array of n elements]
	  OUTPUT : Boolean value after comparision
	  IMPLEMENTATION:Perform split with the array already defined and the user input array
	  and then merge the split arrays of both the arrays and compare the arrays and return boolean value.
	*/
	var evaluateMerge = function(userInput, q1, q2){
		//console.log("merge evaluation", userInput, q1, q2);
		var comparedResult;
		comparedResult = E.Utilities.array.isEqual(userInput, ms.merge(q1, q2));
		return comparedResult;
	}
	/*
	  EVALUATE MERGE SORT FUNCTION
	  INPUT: Two arrays : User input array [array of n elements]
	  					  Input array already defined[array of n elements]
	  OUTPUT : Boolean value after comparision
	  IMPLEMENTATION:Perform merge sort with the array already defined and the user input array
	  and compare the arrays and return boolean value.
	*/
	var evaluateMergeSort = function(userInput, inputArray){
		var comparedResult;
		comparedResult = E.Utilities.array.isEqual(userInput, ms.mergeSort(inputArray));
		return comparedResult;
		//console.log(comparedResultL);
	}



		//Switch case  for ctx string split merge and mergesort
			switch(ctx){
				case 'split':
				var splitResult = evaluateSplit(uInput, inputArray);
				return splitResult;
				break;
				case 'merge':
				var mergeResult = evaluateMerge(uInput, inputArray);
				return mergeResult;
				break;
				case 'mergesort':
				var mergeSortResult = evaluateMergesort(uInput, inputArray);
				return mergeSortResult;
				break;
			}


}

// utilities on array to compare
E.Utilities.array = {};
E.Utilities.array.isEqual = function (value, other) {

	// Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var compare = function (item1, item2) {

		// Get the object type
		var itemType = Object.prototype.toString.call(item1);

		// If an object or array, compare recursively
		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
			if (!isEqual(item1, item2)) return false;
		}

		// Otherwise, do a simple comparison
		else {

			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) return false;

			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === '[object Function]') {
				if (item1.toString() !== item2.toString()) return false;
			} else {
				if (item1 !== item2) return false;
			}

		}
	};

	// Compare properties
	if (type === '[object Array]') {
		for (var i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) return false;
		}
	} else {
		for (var key in value) {
			if (value.hasOwnProperty(key)) {
				if (compare(value[key], other[key]) === false) return false;
			}
		}
	}

	// If nothing failed, return true
	return true;

};

//console.log(E.mergeSort(E.testData, 'split'));
//document.getElementById("demo").innerHTML=mergeSort(input);

 //End of API
console.log("E loaded");
})(Mergesort);
