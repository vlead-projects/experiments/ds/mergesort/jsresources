// Mergesort data after parsing from the catalog
// [TODO] still layout and storyboard details not added
window.Page = 
{

	id:"page-id",
	slug: "page-slug",
	title: "",
	description: "",
	theme: "",
	panes: [{
		id:"pane1",
		type:"pane",
		
		blocks:[{
			"type":"artefact:inline",
			"name": "Section Heading",
			"title":"Practice Exercise",
			"toElement": "h2",
			"styleProps": {
						"font-family": "sans-serif",
						"font-color":"black",
						"display": "block",
					    "margin-top": "0.67em",
					    "margin-bottom": "0.67em",
					    "margin-left": "0",
					    "margin-right":" 0",
					    "font-weight": "bold"	
					}
		},
		{   
			"type":"artefact:$reqArtefact",
			"name":"Array generator widget",
			"title":"Generate Array",
			"toElement": "button", 
			"toType": "widget",
			"styleProps": {
					    "margin-top": "0.67em",
					    "margin-bottom": "0.67em",
					    "margin-left": "0",
					    "margin-right":" 0",
					    "font-weight": "bold"	
					}
			
		},
		{
			"type":"artefact:inline",
			"name":"Sub-section Heading",
			"title":"Puzzle",
			"toElement": "h3",
			"styleProps": {
						"font-family": "Times New Roman",
						"font-color":"black"
					}

		},
		{
			"type":"artefact:inline",
			"name":"Sub-section Heading",
			"title":"Result",
			"toElement": "h4", 
			"styleProps": {
						"font-family": "Times New Roman",
						"font-color":"black"
					}
		}]
	},
	{
		id:"pane2",
		type:"pane",
		
		blocks:[{
			"type":"artefact:inline",
			"name": "Section Heading",
			"title":"Test your understanding of split",
			"toElement": "h3",
			"styleProps": {
						"font-family": "Times New Roman",
						"font-color":"black"
					}
		},
		{   
			"type":"artefact:inline",
			"name": "sub-Section Heading",
			"title":"Description",
			"toElement": "p", 
			"styleProps": {
						"align": "left"
						
					}
		},
		{
			"type":"artefact:inline",
			"name":"",
			"title":"left",
			"toElement": "label",
			"styleProps": {
						"font-family": "Times New Roman",
						"font-color":"black"
					}

		},
		{
			"type":"artefact:inline",
			"name":"",
			"title":"right",
			"toElement": "label",
			"styleProps": {
						"font-family": "Times New Roman",
						"font-color":"black"
					}

		},
		{
			"type":"artefact:inline",
			"name":"",
			"title":"Result",
			"toElement": "label"
		}]
			
	}],
	
	published: false,
	created:"",
	creator:"",
	creatorId: "",
	showNav: true
}
