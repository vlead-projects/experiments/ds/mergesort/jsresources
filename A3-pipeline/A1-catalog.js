/*
** question: what is the diff b/w SCAT:reqa
SCAT:imga

	"split-even-show": {
		"id": "split-even-show",
		"header": "Show the even split���",
		"text": "Provide a diagram where an input array of 8 elements is split into two arrays of 4 contiguous elements.",
		"properties": {
			"CUSTOM_ID": "split-even-show",
			"SCAT": "reqa",
			"TYPE": "Image"
		},
		"divHtml": "\n\n<p>\nProvide a diagram where an input array of 8 elements is\nsplit into two arrays of 4 contiguous elements. \n</p>\n"
	},
	"merge-sort-intro-img": {
		"id": "merge-sort-intro-img",
		"header": "Image of Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-sort-intro-img",
			"SCAT": "imga",
			"CAPTION": "Merge Sort Cirle"
		},
		"divHtml": "\n\n"
	},

	*/

window.CatalogA1 = {
	"merge-sort-exp": {
		"id": "merge-sort-exp",
		"header": "Preamble���",
		"text": "",
		"properties": {
			"SCAT": "pmbl",
			"CUSTOM_ID": "merge-sort-exp"
		},
		"tasks": []
	},
	"merge-sort-steps": {
		"id": "merge-sort-steps",
		"header": "Merge Sort Involves the following steps���",
		"text": "Split - Splits the input array in two halves, Merge Sort - Perform mergesort on the two halves, Merge - Merge the two halves to get the sorted array",
		"properties": {
			"SCAT": "txta",
			"CUSTOM_ID": "merge-sort-steps"
		},
		"divHtml": "\n\n<p>\nSplit - Splits the input array in two halves, \nMerge Sort - Perform mergesort on the two halves, \nMerge - Merge the two halves to get the sorted array\n</p>\n"
	},
	"merge-sort-intro-pic": {
		"id": "merge-sort-intro-pic",
		"header": "Picture of Merge sort���",
		"text": "The image is a flowchart of the mergesort that is illustrated with an input array at the top that is split; mergesort applied on the split arrays; and the split arrays merged in the downstream to produce a sorted array.",
		"properties": {
			"CUSTOM_ID": "merge-sort-intro-pic",
			"SCAT": "reqa",
			"TYPE": "Image"
		},
		"divHtml": "\n\n<p>\nThe image is a flowchart of the mergesort that is\nillustrated with an input array at the top that is split;\nmergesort applied on the split arrays; and the split arrays\nmerged in the downstream to produce a sorted array.\n</p>\n"
	},
	"merge-sort-intro-video": {
		"id": "merge-sort-intro-video",
		"header": "Video on Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-sort-intro-video",
			"SCAT": "vida",
			"CAPTION": "Merge sort algorithm"
		},
		"divHtml": "\n\n"
	},
	"merge-sort-intro-img": {
		"id": "merge-sort-intro-img",
		"header": "Image of Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-sort-intro-img",
			"SCAT": "imga",
			"CAPTION": "Merge Sort Cirle"
		},
		"divHtml": "\n\n"
	},
	"merge-sort-intro": {
		"id": "merge-sort-intro",
		"header": "Merge Sort Introduction���",
		"text": "",
		"properties": {
			"SCAT": "task",
			"CUSTOM_ID": "merge-sort-intro"
		},
		"artifacts": [
			"merge-sort-steps",
			"merge-sort-intro-pic",
			"merge-sort-intro-video",
			"merge-sort-intro-img"
		]
	},
	"merge-sort-unit": {
		"id": "merge-sort-unit",
		"header": "Merge Sort Unit���",
		"text": "",
		"properties": {
			"SCAT": "lu",
			"CUSTOM_ID": "merge-sort-unit"
		},
		"tasks": [
			"merge-sort-intro"
		]
	},
	"split-even": {
		"id": "split-even",
		"header": "Split even number of elements���",
		"text": "When an an array with even number of elements is split, the output is two arrays with equal number of elements.",
		"properties": {
			"CUSTOM_ID": "split-even",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nWhen an an array with even number of elements is split, the\noutput is two arrays with equal number of elements. \n</p>\n"
	},
	"split-even-show": {
		"id": "split-even-show",
		"header": "Show the even split���",
		"text": "Provide a diagram where an input array of 8 elements is split into two arrays of 4 contiguous elements.",
		"properties": {
			"CUSTOM_ID": "split-even-show",
			"SCAT": "reqa",
			"TYPE": "Image"
		},
		"divHtml": "\n\n<p>\nProvide a diagram where an input array of 8 elements is\nsplit into two arrays of 4 contiguous elements. \n</p>\n"
	},
	"split-odd": {
		"id": "split-odd",
		"header": "Split odd number of elements���",
		"text": "When an an array with odd number of elements is split, the output is two arrays with one array containing one element greater than the other.",
		"properties": {
			"CUSTOM_ID": "split-odd",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nWhen an an array with odd number of elements is split, the\noutput is two arrays with one array containing one element\ngreater than the other.\n</p>\n"
	},
	"split-odd-show": {
		"id": "split-odd-show",
		"header": "Show the odd split���",
		"text": "Provide a diagram where an input array of 7 elements is split into two arrays of 4 and 3 contiguous elements.",
		"properties": {
			"CUSTOM_ID": "split-odd-show",
			"SCAT": "reqa",
			"TYPE": "Image"
		},
		"divHtml": "\n\n<p>\nProvide a diagram where an input array of 7 elements is\nsplit into two arrays of 4 and 3 contiguous elements. \n</p>\n"
	},
	"split-illustrate-task": {
		"id": "split-illustrate-task",
		"header": "Illustrate Split���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "split-illustrate-task",
			"SCAT": "task"
		},
		"artifacts": [
			"split-even",
			"split-even-show",
			"split-odd",
			"split-odd-show"
		]
	},
	"split-illustrate-lu": {
		"id": "split-illustrate-lu",
		"header": "Split���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "split-illustrate-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"split-illustrate-task"
		]
	},
	"split-exercise-even": {
		"id": "split-exercise-even",
		"header": "Split on even array���",
		"text": "An even array is provided, perform the split into two equal halves.",
		"properties": {
			"CUSTOM_ID": "split-exercise-even",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nAn even array is provided, perform the split into two equal\nhalves.\n</p>\n"
	},
	"split-even-interactive": {
		"id": "split-even-interactive",
		"header": "Perform the split���",
		"text": "Provide an artifact where a student interactively performs the split on an array containing even number of elements.",
		"properties": {
			"CUSTOM_ID": "split-even-interactive",
			"SCAT": "reqa",
			"TYPE": "InteractiveJS"
		},
		"divHtml": "\n\n<p>\nProvide an artifact where a student interactively performs\nthe split on an array containing even number of elements.\n</p>\n"
	},
	"split-exercise-odd": {
		"id": "split-exercise-odd",
		"header": "Split on an odd array���",
		"text": "An array with odd number of elements is provided, perform the split into two parts where one of the arrays has one element more than the other.",
		"properties": {
			"CUSTOM_ID": "split-exercise-odd",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nAn array with odd number of elements is provided, perform\nthe split into two parts where one of the arrays has one\nelement more than the other.\n</p>\n"
	},
	"split-odd-interactive": {
		"id": "split-odd-interactive",
		"header": "Perform the split���",
		"text": "Provide an artifact where a student can interactively perform the split on an array containing odd number of elements.",
		"properties": {
			"CUSTOM_ID": "split-odd-interactive",
			"SCAT": "reqa",
			"TYPE": "InteractiveJS"
		},
		"divHtml": "\n\n<p>\nProvide an artifact where a student can interactively\nperform the split on an array containing odd number of\nelements.\n</p>\n"
	},
	"split-exercise-task": {
		"id": "split-exercise-task",
		"header": "Split Exercise���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "split-exercise-task",
			"SCAT": "task"
		},
		"artifacts": [
			"split-exercise-even",
			"split-even-interactive",
			"split-exercise-odd",
			"split-odd-interactive"
		]
	},
	"split-exercise-lu": {
		"id": "split-exercise-lu",
		"header": "Perform Split���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "split-exercise-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"split-exercise-task"
		]
	},
	"merge-describe": {
		"id": "merge-describe",
		"header": "Merge elements���",
		"text": "Given two arrays of numbers, they are merged to produce a single array that is sorted where the first element in the merged array is the least and the last element in the array is the highest.",
		"properties": {
			"CUSTOM_ID": "merge-describe",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nGiven two arrays of numbers, they are merged to produce a\nsingle array that is sorted where the first element in the\nmerged array is the least and the last element in the array\nis the highest. \n</p>\n"
	},
	"merge-show": {
		"id": "merge-show",
		"header": "Show the merge���",
		"text": "Provide a diagram where two input arrays are merged producing an array that is sorted.",
		"properties": {
			"CUSTOM_ID": "merge-show",
			"SCAT": "reqa",
			"TYPE": "Image"
		},
		"divHtml": "\n\n<p>\nProvide a diagram where two input arrays are merged\nproducing an array that is sorted. \n</p>\n"
	},
	"merge-illustrate-task": {
		"id": "merge-illustrate-task",
		"header": "Illustrate Merge���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-illustrate-task",
			"SCAT": "task"
		},
		"artifacts": [
			"merge-describe",
			"merge-show"
		]
	},
	"merge-illustrate-lu": {
		"id": "merge-illustrate-lu",
		"header": "Merge���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-illustrate-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"merge-illustrate-task"
		]
	},
	"merge-exercise": {
		"id": "merge-exercise",
		"header": "Merge two arrays���",
		"text": "Two arrays of integers are provided, perform the merge to produce a single array that is sorted.",
		"properties": {
			"CUSTOM_ID": "merge-exercise",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nTwo arrays of integers are provided, perform the merge to\nproduce a single array that is sorted.\n</p>\n"
	},
	"merge-exercise-interactive": {
		"id": "merge-exercise-interactive",
		"header": "Perform the Merge���",
		"text": "Provide an artifact where a student interactively performs the merge on arrays.",
		"properties": {
			"CUSTOM_ID": "merge-exercise-interactive",
			"SCAT": "reqa",
			"TYPE": "InteractiveJS"
		},
		"divHtml": "\n\n<p>\nProvide an artifact where a student interactively performs\nthe merge on arrays.\n</p>\n"
	},
	"merge-exercise-task": {
		"id": "merge-exercise-task",
		"header": "Merge Exercise���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-exercise-task",
			"SCAT": "task"
		},
		"artifacts": [
			"merge-exercise",
			"merge-exercise-interactive"
		]
	},
	"merge-exercise-lu": {
		"id": "merge-exercise-lu",
		"header": "Perform Merge���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "merge-exercise-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"merge-exercise-task"
		]
	},
	"merge-sort-text": {
		"id": "merge-sort-text",
		"header": "Merge Sort on an Array���",
		"text": "Given an unsorted, split and merge are performed recursively to produce a sorted array.",
		"properties": {
			"CUSTOM_ID": "merge-sort-text",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nGiven an unsorted, split and merge are performed recursively\nto produce a sorted array.\n</p>\n"
	},
	"merge-sort-show": {
		"id": "merge-sort-show",
		"header": "Show the Merge Sort���",
		"text": "Provide an interactive resource that takes a user input of integers and produces an output sorted array.",
		"properties": {
			"CUSTOM_ID": "merge-sort-show",
			"SCAT": "reqa",
			"TYPE": "InteractiveJS"
		},
		"divHtml": "\n\n<p>\nProvide an interactive resource that takes a user input of\nintegers and produces an output sorted array.\n</p>\n"
	},
	"mergesort-illustrate-task": {
		"id": "mergesort-illustrate-task",
		"header": "Illustrate Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "mergesort-illustrate-task",
			"SCAT": "task"
		},
		"artifacts": [
			"merge-sort-text",
			"merge-sort-show"
		]
	},
	"mergesort-illustrate-lu": {
		"id": "mergesort-illustrate-lu",
		"header": "Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "mergesort-illustrate-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"mergesort-illustrate-task"
		]
	},
	"mergesort-exercise": {
		"id": "mergesort-exercise",
		"header": "Sort an array using MergeSort���",
		"text": "Perform split and merge on the given array to produce a sorted array.",
		"properties": {
			"CUSTOM_ID": "mergesort-exercise",
			"SCAT": "txta"
		},
		"divHtml": "\n\n<p>\nPerform split and merge on the given array to produce a\nsorted array.\n</p>\n"
	},
	"mergesort-interactive": {
		"id": "mergesort-interactive",
		"header": "Perform the Merge Sort���",
		"text": "Provide an artifact where a student interactively performs the split and merge on an array containing integers.",
		"properties": {
			"CUSTOM_ID": "mergesort-interactive",
			"SCAT": "reqa",
			"TYPE": "InteractiveJS"
		},
		"divHtml": "\n\n<p>\nProvide an artifact where a student interactively performs\nthe split and merge on an array containing integers.\n</p>\n"
	},
	"mergesort-exercise-task": {
		"id": "mergesort-exercise-task",
		"header": "Merge Sort Exercise���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "mergesort-exercise-task",
			"SCAT": "task"
		},
		"artifacts": [
			"mergesort-exercise",
			"mergesort-interactive"
		]
	},
	"mergesort-exercise-lu": {
		"id": "mergesort-exercise-lu",
		"header": "Perform Merge Sort���",
		"text": "",
		"properties": {
			"CUSTOM_ID": "mergesort-exercise-lu",
			"SCAT": "lu"
		},
		"tasks": [
			"mergesort-exercise-task"
		]
	}
}