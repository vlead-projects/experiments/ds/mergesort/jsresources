// Let's build the helpers for building the views

document.onload = (function(){
	console.log("loaded with front end catalog", ncbsCat);

	// what do we need?
	// A frontend catalog which will be referred by the layout 
	// components - thinking out loud, cant say much now
	/*
	jus pasting the model here, so i can think,
	i guess i need $ref
	ncbsCat = {
		"id": "cat1",
		"type": "chapter",
		"title":"Identity",
		"description":" may be some curatorial note, or $ref(this.catalog, id)",
		"catalog": {
			"identity-intro": {
			"id": "id-intro",
			"type": "inline:text",
			"resource": {
				"id": "intro-id",
				"type": "text",
				"data": "In 1944, Homi Bhabha informed A.V. Hill, then Secretary of the Royal Society, that the Sir Dorabji Tata Trust had agreed to sponsor his proposal for a fundamental research institute in physics...."	
			}
		},
		"1-space-intro": {
			"id": "space-intro",
			"type": "inline:text",
			"resource": {
				"id": "intro-space",
				"type": "text",
				"data": "Nation building in the decades after Indian independence came with this patina of worship. The process included large scale science projects like dams and power plants, schools and universities. And it included new research centres....."	
			}
		},
*/


/*
*/

/*
Example: 
This whole below this looks like comes up as models
now we'll need types defined and bind with thhese models?

*/
M = {};
M.models = {};

//layout broker should check for column arguments and

// legal args 1 to 12
M.models.Layout = function(left, center, right){
	this.type = "section";
	this.layout = {};
	this.layout.el = {
		left: [],
		center: [],
		right: []
	}
	this.layout.columns = this.setLayout(left, center, right)
}

//Sections will create the global layout objects for stories
M.models.Layout.prototype = {
	getLayout: function(){
		try{
			return this.layout.columns;
		}
		catch(error){
			console.log(error, "error");
		}
	},
	setLayout: function(left, center, right){
		return {
			left: left,
			center: center,
			right:right
		}

	},
	addBlocks: function(el, label){

		this.layout.el[label].push(el);
		return this.layout;
	}
}

// content broker to access resources from catalog look up
M.models.Resource = function(catalog, id){
	this.data = catalog[id];
}

M.models.Resource.prototype = {
	//will return an array,
	// more on grammar later...

	getType: function() {
		var types = this.data.type.split(":");
		switch(types.length){
			case 2:
				return types[1];
				break;
			case 3:
			    return types[2];
			    break;
			default:
				return types[1];
		}
	},

	Get: function() {
		return this.data.resource;
	},
	Type: function() {
		return this.data.resource.type;
	},
	Data: function() {
		return this.data.resource.data;
	},
	Id: function() {
		return this.data.resource.id;
	}
}



// Broker object to interact with story and make,
// necessary frontend representations
M.models.storyBroker = function(arg){
	this.type = "repr";
	this.story = arg;
}

M.models.storyBroker.prototype = {
	getStory: function(){
		return this.story;
	},
	getCatalog: function(){
		return this.story.catalog;
	}, 
	getChildren: function(){
		return this.story.children;
	},
	/*makeBlocks: function(cat){
		return this.getChildren().map(function(child){
			return new M.models.Layout(6, 0, 6);
		}, this);
		
	},*/
	resolveChildren: function(){
		//console.log(this);
		return this.getChildren().map(function(child){
			return new M.models.Resource(this.getCatalog(), child.$ref);
		}, this);
		
	}
}


//================================================

// Here we are organising the templates
// using https://github.com/joestelmach/laconic/

M.templates = {};

M.templates.baseTypeTemplates = {
	textTpl: function(content){
		return $.el.p({name: content.name}, $.el.div(content.content));
	},
	textHeadingTpl: function(content){
		return $.el.h1({name: content.name}, content.content);
	},
	imgTpl: function(content){
		return $.el.img({src: content.src, name: content.name})
	},
	bquoteTpl: function(content){
		return $.el.blockquote({name: content.name}, content.content);
	},
	galleryTpl: function(list){
		return list.map(function(item){
			if(item.type === 'image'){
				return this.imgTpl({src: item.src, name: item.id});
			}
			
		}, this);
	}
};

M.templates.layoutTemplates = function(section, content){
	if(section.getLayout().left > 0 && section.getLayout().center > 0 && section.getLayout().right > 0){
		return $.el.div({class: "row"}, 
				$.el.div({class: "col-md-"+section.getLayout().left, name: "left"}, content.left),
				$.el.div({class: "col-md-"+section.getLayout().center, name: "center"}, content.center),
				$.el.div({class: "col-md-"+section.getLayout().right, name: "right"}, content.right) );
	}	
}

//======================================================
// Here views!
M.views = {};

/*
Example:
var ex1 = new M.views.textView("text", M.templates.textTpl, "intro")
ex1.render({data: "Hello world", id: "name"});
will return..
compiled html node
*/


M.views.baseTypeView = function(type, tpl, id){
	this.tpl = tpl;
	this.id = id;
	this.type = type;
}

M.views.baseTypeView.prototype = {
	render: function(data){
		return this.tpl({content: data.data, name: this.id});
	}
}



//===========================================================================

M.views.pageView = function(chapter, el){
	this.chapter = chapter;
	this.el = el;
	this.title = chapter.story.title
	this.children = this.chapter.resolveChildren();
}

M.views.pageView.prototype = {
	init: function(){
		console.log("initialized");
		this.render();
	},
	controller: function(){
		//console.log("controller", this.blocks, this.children);

	},
	templateResolver: function(type){
		// choose relevant template base on type
		switch(type){
			case 'text':
			return M.templates.baseTypeTemplates.textTpl;
			break;

			case 'image':
			return M.templates.baseTypeTemplates.imgTpl;
			break;

			case 'blockquote':
			return M.templates.baseTypeTemplates.bquoteTpl;
			break;

			case 'gallery':
			return M.templates.baseTypeTemplates.galleryTpl;

			default:
			return M.templates.baseTypeTemplates.textTpl;
		} 
	},
	makeSection: function(section, content){
		return new M.templates.layoutTemplates(section, content);
	},
	render: function(){
		document.getElementById(this.el).innerHtml = "";
		// pass reolved type and template to create html elements 
		//TODO: nodes shud be dictionary instead of array
		var node = []
		_.each(this.children, function(item){
			//console.log(this.typeResolver(item), this.templateResolver(this.typeResolver(item)));
			var blockContent = new M.views.baseTypeView(item.getType(), 
												this.templateResolver(item.getType()), 
												item.Id());
			
			// call render on the block content templates
			node.push(blockContent.render({
						data: item.Data()
						}));			
		}, this)
		//console.log(node);
		// This has to be dynamic based on labels or names,
		// as a object
		//TODO: layout options needs to be dynamically accepted from the init

		this.layout = this.makeSection(new M.models.Layout(4, 4, 4), 
									{
										left: node[0], 
										center: node[1], 
										right: node[2] 
									});
		//this.gallery = this.makeSection(new M.models.Layout(), )
		document.getElementById(this.el).append(this.layout);
		console.log("rendered", this.layout);
	}
}



//instantiate

var story = new M.models.storyBroker(ncbsCat);
pageViewInstance = new M.views.pageView(story, 'page');
pageViewInstance.init();

console.log("page view instantiated, here is the page object", pageViewInstance);

})();