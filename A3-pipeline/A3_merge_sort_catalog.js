
/*

# STAGE A3 for Mergesort
After parsing from A1, below is a version of the experiment structure for mergesort
TODO:Resolved decoration @annotations are added, to have a Flatten view.

### THE RESOURCE STRUCTURE
Will be empty object for requirement artefact
and will be below structure for other types
** Here Type is Content Type
```
resource =	{
 			"name": "kidekal Basava",
            "creator": "bhanu",
            "creatorId": "2",
            "created": 1526044899,
            "description": null,
            "src": "2_1526044899_kidekal-basava.mp4",
            "attributes": [],
            "type": "video",
            "thumb": "2_1526044899_thumb_kidekal-basava.png"
        }
```

 ### THE ARTEFACT STRUCTURE

```
artefact = {
****	//Questions: so this type here will be what??
****

	"type":"$refArtefact",
	"id":"artefact1",
	"_comment": "This is a video to explain the practise",
	"title":"Practice Exercise",
	"resource": {
 			"name": "kidekal Basava",
            "creator": "bhanu",
            "creatorId": "2",
            "created": 1526044899,
            "description": null,
            "src": "2_1526044899_kidekal-basava.mp4",
            "attributes": [],
            "type": "video",
            "thumb": "2_1526044899_thumb_kidekal-basava.png"
        }
}
```

### THE CATALOG STRUCTURE

```
Catalog = {
	"a1": {
	"type":"$refArtefact",
	"id":"resource1",
	"_comment": "This is a video of kodekal basava",
	"resource": {
 			"name": "kodekal Basava",
            "creator": "bhanu",
            "creatorId": "2",
            "created": 1526044899,
            "description": null,
            "src": "2_1526044899_kidekal-basava.mp4",
            "attributes": [],
            "type": "video",
            "thumb": "2_1526044899_thumb_kidekal-basava.png"
        }
	},
	"a2": {...},
	"a3": {...},...,
}

```


Adding new property "@annotations" to artefacts, 
there might be key ui / ux decisions
which may be documented through @annotations 
and domain specific issue management language.


### THE @annotations STRUCTURE v0.0.0
while adding it has evolved into v0.0.2

```
{
	"creator": "salus sage",
	"creatorId": "2",
	"created": 1526044899,
	"body": {
		assignedTo:"",
		text:"",
		toElement:"",
		designProps:"",
**** where labels might be specifc to domain sme, id, ui, or any custom?
****
		label:{}
	}
**** An hash param, unique name reference?
****
	"target": {},
}
```

======================================================

=====================
From The SME - ID Doc the content types needed, can be extended
Each of the type will have a View(JS) and Template(HTML) 
['text', 'image', 'video', 'table', 'plugin']

Each of the above types need to extend further,
to address UI / UX concerns.
ex1:
type- text:h1 or text:heading1

global css - heading1 {
	font-family: Roboto;
}

ex2:
type- image:360

js view - will provide a callback and source code
to handle.

ex3: 
type- video:youtube

template will handle embeds

===============================

var BaseType = Backbone.Model.extend({
    defaults: {
      tags: [],
      title: "",
      attr: {},
      type: ''
    },
    initialize: function() {
    }
});

var Text = BaseType.extend({
    defaults: _.extend({
      data: ""
    }, BaseType.prototype.defaults),
    initialize: function() {
      BaseType.prototype.initialize.call(this, arguments);
    }
});

var Image = BaseType.extend({
    defaults: _.extend({
      src: ""
    }, BaseType.prototype.defaults),
    initialize:function() {
      BaseType.prototype.initialize.call(this, arguments);
    }
});

Then in Views, instantiate a template for each type
for render.
==============================
Model for the Screen abstraction
Pages = Collection of Page
Page = 
{
	id:"page-id",
	slug: "page-slug",
	title: "",
	description: "",
	theme: ""
	layout: [{
				slug: "",
				id: "layout",
				panes: [{
					id:"",
					type:"pane",
					blocks: []
			}],
	published: false,
	created:"",
	creator:"",
	creatorId: "",
	showNav: true
}
Blocks - content and template (by type) compiled with attributes
Panes - Blocks with layout


# Now below you have the exeriment structure 
as it may look at A3 for one screen
or few artefacts.
 
 */

(function(){ 
	A3=window.A3 || {};
	A3.catalog = {
	"title": "MergeSort Experiment",
	"type": "exp",
	"id": "mergesort-2",
	"_comment": "This is a experiment containing learning units",
	"learning_unit": {
		"type": "lu",
		"_comment": "this is a learning unit, containing tasks",
		"tasks": [{
				"id": "t1",
				"type": "task",
				"_comment": "this is a task, containing artefacts",
				"artefacts": [{
						"type": "artefact:inline",
						"id": "artefact1",
						"_comment": "This is a artefact containing resources",
						"resource": {
				 			"name": "Section Heading",
				            "creator": "bhanu",
				            "creatorId": "2",
				            "created": 1526044899,
				            "description": null,
				            "attributes": [],
				            "type": "text",
				            "text": "Practice Exercise"
				        },
						"@annotations": [{
							"id": "1",
							"type": "annotation",
							"body": {
								"_who": ["ui"],
								"_comment": "Make this centre aligned",
								"toType": "Heading 1",
								"styleProps": {
									"font-family": "sans-serif"
								},
								"label": {
									"id": "ui"
								},
								"assignedTo": "owner id"
							},
							"target": {
								"slug": "heading-artefact1"
							},
							"creator": "salus sage",
							"creatorId": "2",
							"created": 1526044899
						}]
					},
					{
						"type": "artefact:req",
						"id": "artefact2",
						"title": "Generate Array",
						"resource": {},
						"_comment": "Resource missing from catalog",
						"@annotations": [{
							"id": "2",
							"type": "annotation",
							"body": {
								"_who": ["id", "ui"],
								"_comment": "Click on button should list integers of length even or odd randomly ",
								"toType": "widget",
								"styleProps": {},
								"scripts": []
							},
							"label": {
								"id": "ux"
							},
							"assignedTo": "owner id",

							"target": {
								"slug": "widget-artefact2"
							},
							"creator": "salus sage",
							"creatorId": "2",
							"created": 1526044899
						}]
					}

				]
			},
			{
				"id": "t2",
				"type": "task",

				"artefacts": [{
						"type": "artefact:inline",
						"id": "artefact1",
						"resource": {
				 			"name": "Section Heading",
				            "creator": "bhanu",
				            "creatorId": "2",
				            "created": 1526044899,
				            "description": null,
				            "attributes": [],
				            "type": "text",
				            "text": "Test your understanding of Split"
				        },
						"@annotations": [{
							"id": "3",
							"type": "annotation",
							"body": {
								"_who": ["ui"],
								"_comment": "Block Heading",
								"toType": "Heading 3",
								"styleProps": {
									"font-family": "sans-serif"
								},
								"label": {
									"id": "ui"
								},
								"assignedTo": "owner id"
							},
							"target": {
								"slug": "heading-artefact1"
							},
							"creator": "salus sage",
							"creatorId": "2",
							"created": 1526044899
						}]
					}, {
						"type": "artefact:inline",
						"id": "split-test-instruction",
						"resource": {
				 			"name": "Block Description",
				            "creator": "bhanu",
				            "creatorId": "2",
				            "created": 1526044899,
				            "description": null,
				            "attributes": [],
				            "type": "text",
				            "text": "Here you will have to split the above list and enter in inputs"
				        },
						"@annotations": [{
							"id": "4",
							"type": "annotation",
							"body": {
								"_who": ["ui"],
								"_comment": "Block Description",
								"toType": "paragraph",
								"styleProps": {
									"font-family": "sans-serif"
								},
								"label": {
									"id": "ui"
								},
								"assignedTo": "owner id"
							},
							"target": {
								"slug": "paragraph-split-test-instruction"
							},
							"creator": "salus sage",
							"creatorId": "2",
							"created": 1526044899
						}]
					}, {
						"type": "artefact:req",
						"id": "split-test",
						"resource": {},
						"_comment": "Compute User test for Split understanding, repeat test and score.",
						"@annotations": [{
							"id": "5",
							"type": "annotation",
							"body": {
								"_who": ["id", "ui"],
								"_comment": "two fields left & and right, and Display pass or fail,space seperated list,in even and odd length ",
								"toType": "widget",
								"styleProps": {
									"font-family": "sans-serif"
								},
								"scripts": [],
								"label": {
									"id": "ux"
								},
								"assignedTo": "owner id"
							},
							"target": {
								"slug": "widget-split-test"
							},
							"creator": "salus sage",
							"creatorId": "2",
							"created": 1526044899
						}]
					}

				]

			}
		]
	}
}
})()
