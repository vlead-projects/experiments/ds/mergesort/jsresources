
// NCBS 13 ways catalog

/* may be getting somewhere with chapter, children (sections)... oooohh
*/

	window.ncbsCat = {
		"id": "cat1",
		"type": "chapter",
		"title":"Identity",
		"description":" may be some curatorial note, or $ref(id)",
		"children": [{
			"id": "section-curatorialnote",
			"type": "section",
			"title": "",
			"$ref": "identity-intro"
		},
		{
			"id": "section-2-intro",
			"type": "section",
			"title": "",
			"$ref": "1-space-intro"
		},
		{
			"id": "section-2-lead",
			"type": "section",
			"title": "",
			"$ref": "1-space-blockquote"
		}],
		"catalog": {
			"identity-intro": {
			"id": "id-intro",
			"type": "inline:text",
			"resource": {
				"id": "intro-id",
				"type": "text",
				"data": "In 1944, Homi Bhabha informed A.V. Hill, then Secretary of the Royal Society, that the Sir Dorabji Tata Trust had agreed to sponsor his proposal for a fundamental research institute in physics...."	
			}
		},
		"1-space-intro": {
			"id": "space-intro",
			"type": "inline:text",
			"resource": {
				"id": "intro-space",
				"type": "text",
				"data": "Nation building in the decades after Indian independence came with this patina of worship. The process included large scale science projects like dams and power plants, schools and universities. And it included new research centres....."	
			}
		},
		"1-space-blockquote":{
			"id": "space-feat",
			"type": "inline:text:blockquote",
			"resource": {
				"id": "bq-space",
				"type": "text",
				"data": "We are now entering into an age when scientists begin to function like the high priests of old, who looked after the sacred mysteries;....."	
			}
		},
		"1-space-ps4": {
			"id": "slide-ps4",
			"name": "_1-Space-PS4",
			"type": "req:image:gallery",
			"resource": {
				"id": "slide-ps4",
				"type": "slide",
				"data": [{
					"id": "1-Space-PS4-1",
					"type": "image",
					"api": "domain+/api/tags/id",
					"src": "http://archives.ncbs.res.in/files/fullsize/39ab5305eb5340036e5c59469cc6b1e2.jpg",
					"caption": "An extract from the 1960-65 Third Five Year Plan, discussing the proposal to build a National Biological Laboratory. By the next plan period, Palampur was the selected location. But the laboratory never materialized, partly because of a disagreement on the location. ",
					"thumbnail": "http://archives.ncbs.res.in/files/square_thumbnail/39ab5305eb5340036e5c59469cc6b1e2.jpg"
				},
				{
					"id": "1-Space-PS4-2",
					"type": "image",
					"api": "domain+/api/tags/id",
					"src": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg",
					"caption": "An extract from the 1964 CSIR Annual Report, where the new National Biological Laboratory in Palampur finds a mention. The laboratory never materialized, partly because of a disagreement on the location. But that has not stopped it from being mentioned in competitive examination questions to this day. ",
					"thumbnail": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg"
				},
				{
					"id": "1-Space-PS4-3",
					"type": "image",
					"api": "domain+/api/tags/id",
					"src": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg",
					"caption": "An extract from the 1964 CSIR Annual Report, where the new National Biological Laboratory in Palampur finds a mention. The laboratory never materialized, partly because of a disagreement on the location. But that has not stopped it from being mentioned in competitive examination questions to this day. ",
					"thumbnail": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg"
				},
				{
					"id": "1-Space-PS4-4",
					"type": "image",
					"api": "domain+/api/tags/id",
					"src": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg",
					"caption": "An extract from the 1964 CSIR Annual Report, where the new National Biological Laboratory in Palampur finds a mention. The laboratory never materialized, partly because of a disagreement on the location. But that has not stopped it from being mentioned in competitive examination questions to this day. ",
					"thumbnail": "http://archives.ncbs.res.in/files/fullsize/ca66193defafbc495c63a08e5dee5c23.jpg"
				}]
			}
		}

		}
		
	}
