
/*This is the beginning of the library to import for
screen-play/presentation.

=========
*/	
(function(){
A3 = window.A3 || {};

window.CatalogA1 = window.CatalogA1 || {};


/*The function to define the functionality of resource brokering is 
written here
Inputs:catalog-object- {"id":"a1",{"id":"c1","type": "api-call","server": Url,
args ...}}
Output: artefact object - {"id":"a1",type": "api-call","server": Url,
args ...}
The structure is only defined now.The function will be filled up soon*/

/*implementation:
In step1 catalog is passed, which returns a partial function that
can later take the key(or id of catalog) as argument and return 
the object associated
Example:
Catalog1 = A3.makeBroker(A3.dummyCatalog);
keya1 = Catalog1('a1');
print keya1
output:
{"id":"viz-split",
"name":"split-img",
"type":"image/png",
"src":"http://mergesort/split.png"
}

object, string => object 

** question: there is no type in the object returned
*/
A3.makeBroker = function(catalog, id){
	return catalog[id];
	/*return function(id){
		return Object.keys(catalog).map(function(key,index){
			//console.log(key, id);
			if(key === id){
				//console.log("result of broker",key,catalog[key]);
				return catalog[key];
			}
		}).reduce(function(item){
			//console.log(item);
			if (item) {
				return item;
			}
		});
	}*/
		
}

//===================================================================================

/*The function to make or build  an object from the resource.
Input:The Output of the makeBroker function will be input for this function
OutPut:object with the properties depending on the artefact choosen.
Ex:If artefact is required,the properties expected may be url,img.
The structure is only defined now.The function will be filled up soon*/

/*Implementation:Input to makeObject() is object returned from the makeBroker()

Example:A3.makeObject(Catalog1('a1'))
output:
{id: "viz-split", name: "split-img", type: "object", src: "http://mergesort/split.png"}

Insight: This should probably be a reactive object like RxJS or similars*/
A3.makeObject = function(resource1){
	this.type = "object";
	this.content = resource1;
	
}

// Extend the makeObject prototype with getters
// ** question: content object has header? should
// be named heading may be, conflict with A3 glossary
//[TODO]: SHould add try catch for all getters.
A3.makeObject.prototype = {
	getType: function() {
		if(this.content.properties.SCAT === 'reqa'){
			return this.content.properties.TYPE;
		}
		else {
			return this.content.properties.SCAT;
		}
		
	},
	getProps: function() {
		return this.content.properties;
	},
	getHeading: function() {
		return this.content.header;
	},
	getId: function() {
		return this.content.id;
	},
	getContent: function() {
		return this.content.text;
	},
	getDivHtml: function() {
		try {
			return this.content.divHtml;
		}
		catch(error){
			console.log(error, "This Type doesn't have divHtml");
		}
		
	},
	hasTasks: function() {
		
		try {
			return this.content.tasks.length > 0 ? true : false;
		}
		catch(error){
			
			var errorMsg = "Type: "+ this.content.properties.SCAT.toString() + " doesn't have Tasks";
			console.log({err: error, message: errorMsg});
		}	
		
	},
	getTasks: function() {
		try {
			return this.content.tasks;
		}
		catch(error){
			var errorMsg = "Type: "+ this.content.properties.SCAT.toString() + " doesn't have Tasks";
			console.log({err: error, message: errorMsg });
		}
	},
	hasArtifacts: function() {
		try {
			return this.content.artifacts.length > 0 ? true : false;
		}
		catch(error){
		
			var errorMsg = "Type:  doesn't have Artifacts";
			console.log({err: error, message: errorMsg});
		}	
	},
	getArtifacts: function() {
		try {

			return this.content.artifacts;
		}
		catch(error){
			var errorMsg = "Type: "+ this.content.properties.SCAT.toString() + " doesn't have Tasks";
			console.log({err: error, message: errorMsg });
		}
	}
}

//=====================================================================================

/*The function to define a block which contains objects and styles and layout

Input: The array of Objects and style - object - 
{id:"s1",width,height,color...}

Output:block object.
The structure is only defined now.The function will be filled up soon

Implementation:Input to makeBlock() is object returned from the makeObject() and
output:The block object is returned if the type after resolving type
Example:A3.makeBlock(A3.makeObject(Catalog1('a1')), {})
tree:img:{src: "http://mergesort/split.png", alt: ""}
elements = {header: 'h3', text: 'p'}

object - block relationship
*/
// ========================================================================
A3.makeBlock = function(ob, el){
	this.type = "block";
	this.data = ob;
	this.style = el;
	this.children = [];
	this.init = function(){
		this.makeChildren();
		return this.children;
	}
}

// Extending makeblock

A3.makeBlock.prototype = {
	makeChildren: function() {
		console.log(this.data.getType());
		switch (this.data.getType()) {
			case 'txta':
				this.children.push({el: "h3", content: this.data.getHeading()}, {el: "p", content: this.data.getContent(), id: this.data.getId()});
				break;
			case 'task':
				this.children.push({el: "h2", content: this.data.getHeading(), id: this.data.getId()});
				break;
			case 'lu':
				this.children.push({el: "h2", content: this.data.getHeading(), id: this.data.getId()});
				break;
			case 'Image':
				this.children.push({el: "img", content: this.data.getHeading(),id: this.data.getId()});
				break;
			default:
				this.children.push({el: "div", content: this.data.getHeading(), id: this.data.getId()});
				console.log("set default el");

		}
		
	}
}
//================================================================================

/*The function to deifne a pane consisting ofblocks and information 
about style
Input:The block objects and the style information as object
Output: The pane object.
The structure is only defined now.The function will be filled up soon

Implementation:Input is the object returned from makeBlock() and
output is pane object that consists of styles for the block object as width and height
Example:A3.makePane(A3.makeBlock(A3.makeObject(Catalog1('a1'))), {width:"300",height:"300"})
Output:Array(1)0:height:"300"img:{src: "http://mergesort/split.png", alt: ""}width:"300"
======================================
layout and block relationship
layout, attributes, styles processor
*/
A3.makePane = function(b1,style){
	this.type = "pane";
	this.style = style;
	this.blocks = [b1];
	/*	return Object.create({
			type:"pane",
			style: style,
			blocks:[{
				img:b1.getImage(),
				width:style.width,
				height:style.height
				}]
		})*/
	}

A3.makePane.prototype.getBlocks = function(){
	return this.blocks;
}

//===================================================================================

/*The function to define the screen consisting of panes and the layout 
and style information
Input:Array of pane objects,the information about style and layout as object
Output:Screen object or array of screens.
The structure is only defined now.The function will be filled up soon*/
A3.makeScreen=function(p1,style,layout){
	this.type = "screen"
	this.style = style;
	this.layout = layout;
	this.panes = [p1];
	
	/*return Object.create({
		type:"screen",
		style: style,
		layout: layout,
		panes:[p1]
	});*/

}

A3.makeScreen.prototype.getPanes = function() {
	return this.panes;
}
})();