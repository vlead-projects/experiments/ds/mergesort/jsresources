// Merge Sort Algorithm
// purpose: To sort the array in the ascending order using merge sort. 

/// the merge sort API
myw={};  // The entire merge sort algorithm is included in the scope of "myw" object
myw.input = [34, 203, 3, 746, 200, 984, 198, 764, 9]; // input to merge sort algorithm

// The splitfun() Splits the input array in two halves
// Merge sort keeps on dividing the list recursively into two halves until it can no more be divided
myw.splitfun = function (inputArray){
    var middle = parseInt(inputArray.length / 2);//parseInt()  parses a string and returns an integer.
    var left   = inputArray.slice(0, middle); // here slice splits the entire array from beginning to the middle element of the array
    var right  = inputArray.slice(middle, inputArray.length);// here slice splits the entire array from middle element to the end element of the array

    return [left, right];
}

//merge() Merges the two halves to get the sorted array
//merge sort combines the smaller sorted lists keeping the new list sorted too
myw.merge = function(left, right)
{
    var result = [];
    //console.log(result.length, "results length");

    // sorting among the splitted individual elements are conducted
    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            //console.log(left[0], right[0], "first elements of left and right");
            result.push(left.shift());
            //console.log(result, "mergesort: left shift: check result");
        } else {
            result.push(right.shift());
            //console.log(result, "mergesort: right shift: check result");
        }
    }
 
    while (left.length)
        result.push(left.shift());
    //console.log(result, "mergesort: left shift: check result - 2nd while");
 
    while (right.length)
        result.push(right.shift());
    //console.log(result, "mergesort: right shift: check result - 2nd while");
    return result;
}


//The mergeSort() Perform mergesort on the two halves
myw.mergeSort = function (inputArray, switchResult)
{
    
    if (inputArray.length < 2)// if the array has only one element then it is sorted,return
        return inputArray;

    var splitArray = myw.splitfun(inputArray);//the left and right splitted arrays are stored in a single variable named splitArray
    
    if(switchResult == 'split') // this step outputs the split result if the user enters 'split', else it return the sorted merged array
        // performs only split
        return splitArray;
    else 
        // recursively perform split and merge
        return myw.merge(myw.mergeSort(splitArray[0]), myw.mergeSort(splitArray[1])); // in splitArray[0] index lies the left array and in splitArray[1] index lies the right array 
}

console.log(myw.mergeSort(myw.input,'split'));


























